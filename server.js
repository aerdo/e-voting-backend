const express = require("express");
const fs = require("fs");
const cors = require("cors");

const sequelize = require('./app/models/index')
const { Op } = require("sequelize");
const User = require('./app/models/user.model');
const Result = require('./app/models/result.model');
const Log = require('./app/models/log.model');

const createKeys = require("./app/crypto/CreateKeys");
const Decrypt = require("./app/crypto/Decrypt");
const CheckSign = require('./app/crypto/CheckSign');
const PATH_PUBLIC = './app/crypto/keys/PublicKey.txt';

const corsOptions = {
    origin: "http://localhost:3000"
    /*origin: "https://el-vote.herokuapp.com"*/
};
createKeys().then(()=>console.log('Server is ready'));

sequelize.sync().then(()=>{
    console.log('Database is ready')
})

const app = express();

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.sendStatus(200);
})

// получение открытого ключа ЦИК
app.get('/publicKey', (req, res)=>{
    const data = fs.readFileSync(PATH_PUBLIC, {encoding:'utf8', flag:'r'});
    res.send({
        key: data
    })
})

// получение списка всех пользователей (test)
app.get('/user', (req, res)=>{
    User.findAll({order: [
            ['name', 'ASC'],
        ],})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.sendStatus(404);
        });
})

// добавление нового пользователя (имя пользователя и его открытый ключ для проверки подписи)
app.post('/user', (req, res)=>{
    User.create(req.body).then((user)=>{
        res.send({message: 'User created', id: user.dataValues.id});
    }).catch(()=>{
        res.send({message: 'Username must be unique'});
    })
});

// получение результатов
app.get('/results', (req, res)=>{
    Result.findAll()
        .then(data => {
            let resArray = [0,0,0,0];
            data.map((item)=>{
                const id = JSON.parse(JSON.stringify(item)).option;
                resArray[id-1] += 1;
            })
            res.send(`
            <!DOCTYPE html>
              <html lang="ru">
                  <head>
                      <title>Голосование</title>
                      <meta charset="utf-8" />
                  </head>
                  <body>
                      <h4>На данный момент голоса распределены следующим образом:</h4>
                      <ul>
                          <li>Кандидат №1: ${resArray[0]}</li>
                          <li>Кандидат №2: ${resArray[1]}</li>
                          <li>Кандидат №3: ${resArray[2]}</li>
                          <li>Кандидат №4: ${resArray[3]}</li>
                      </ul>
                      <a href="log">Протоколы пользователей</a>
                  </body>
              </html>
            `);
        })
        .catch(err => {
            res.sendStatus(404);
        });
})

// учет голоса (расшифровывание сообщения, проверка подписи, учет голоса)
app.post('/vote',  (req, res)=>{
    const encryptedData = req.body;
    const data = new Uint8Array(encryptedData);
    Decrypt(data).then((plaintext) => {
        const view = new Uint8Array(plaintext);
        let dec = new TextDecoder();
        const userInfo = JSON.parse(dec.decode(view));
        User.findAll({ where: { name: { [Op.like]: userInfo.name }}}).then((userObject)=>{
            if (userObject.length !== 0){
                const temp = JSON.parse(JSON.stringify(userObject))[0];
                const userPublicKeyString = temp.publicKey;
                CheckSign(userPublicKeyString, userInfo.vote, userInfo.signature).then(fl=>{
                    if (fl){
                        Result.create({option: userInfo.vote}).then(()=>{
                            // удаление открытого ключа пользователя
                            const newInfo = {
                                publicKey: 'voted'
                            }
                            User.update(newInfo,{
                                where: { id: temp.id },
                                attributes: ['publicKey']

                            }).then(()=>{
                                res.send({message: 'Vote is counted'});
                            })
                        })
                    }else{
                        // попросить отправить сообщение еще раз
                        User.destroy({
                                where: { id: temp.id }
                            }
                        ).then(()=>{
                            res.send({message: "Error-Sign"});
                        })
                    }
                });
            }else{
                res.send({message: "Error-Not Found"});
            }

        });
    })
})

app.post("/delete-user", (req,res)=>{
    User.findAll({ where: { name: { [Op.like]: req.body.name }}}).then((userObject)=>{
        const temp = JSON.parse(JSON.stringify(userObject))[0];
        User.destroy({
                where: { id: temp.id }
            }
        ).then(()=>{
            res.send({message: "User deleted"});
        })
    })
})

app.post('/log', (req, res)=>{
   Log.create(req.body).then(()=>{
        res.sendStatus(201);
    })
})

app.get('/log', (req, res)=>{
    Log.findAll({attributes: ['userId']}).then((data)=>{
        const temp = JSON.parse(JSON.stringify(data));
        res.send(`
            <!DOCTYPE html>
              <html lang="ru">
                  <head>
                      <title>Протоколы</title>
                      <meta charset="utf-8"/>
                  </head>
                  <body>
                    <h4>Протоколы пользователей</h4>
                    <ul>
                        ${temp.map(item => (`<li><a href="/log/${item.userId}">Пользователь №${item.userId}</a></li>`)).join('')}
                    </ul>
                    <a href="/results">Результаты голосования</a>
                  </body>
              </html>
        `)
    })
})

app.get('/log/:id', (req, res)=>{
    const id = req.params.id;
    Log.findAll({ where: { userId: { [Op.like]: id }}}).then((data)=>{
        if (data.length !== 0){
            const temp = JSON.parse(JSON.stringify(data))[0];
            res.send(`
              <!DOCTYPE html>
              <html lang="ru">
                  <head>
                      <title>Протоколы</title>
                      <meta charset="utf-8"/>
                  </head>
                  <body>
                    <h4>Запись протокола пользователя №${id}</h4>
                    <p style="white-space: pre-line">${temp.log}</p>
                    <a href="/log">Назад</a>
                  </body>
              </html>
            `)
        }
    })
})

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
