const fs = require('fs');
const crypto = require('crypto').webcrypto;

const PATH_PUBLIC = './app/crypto/keys/PublicKey.txt';
const PATH_PRIVATE = './app/crypto/keys/PrivateKey.txt';

async function createKeys(){
    if (!fs.existsSync(PATH_PUBLIC)) {
        generateKeyPair().then(res=>{
            fs.open(PATH_PUBLIC, 'w', (err) => {
                console.log('Public Key Created');
                fs.writeFileSync(PATH_PUBLIC, res[0]);
            });
            fs.open(PATH_PRIVATE, 'w', (err) => {
                console.log('Private Key Created');
                fs.writeFileSync(PATH_PRIVATE, res[1]);
            });
        });
    }
}

async function generateKeyPair() {
    let keyPair = await crypto.subtle.generateKey(
        {
            name: "RSA-OAEP",
            modulusLength: 4096,
            publicExponent: new Uint8Array([1, 0, 1]),
            hash: "SHA-256"
        },
        true,
        ["encrypt", "decrypt"]
    );

    const publicKey = await crypto.subtle.exportKey("jwk", keyPair.publicKey);
    const publicKeyS = JSON.stringify(publicKey);

    const privateKey = await crypto.subtle.exportKey("jwk", keyPair.privateKey);
    const privateKeyS = JSON.stringify(privateKey);

    return [publicKeyS, privateKeyS];
}


module.exports = createKeys;
