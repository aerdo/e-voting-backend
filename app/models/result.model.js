const {Model, DataTypes} = require('sequelize');
const sequelize = require('./index');

class Result extends Model {}
Result.init({
    option: {
        type: DataTypes.INTEGER
    }
},{
    sequelize,
    modelName: 'result',
    freezeTableName: true,
    timestamps: false
})

module.exports = Result;