const {Model, DataTypes} = require('sequelize');
const sequelize = require('./index');

class User extends Model {}

User.init({
    name: {
        type: DataTypes.STRING,
        unique: true
    },
    publicKey: {
        type: DataTypes.STRING(500)
    }
},{
    sequelize,
    modelName: 'user',
    freezeTableName: true,
    timestamps: false
})

module.exports = User;