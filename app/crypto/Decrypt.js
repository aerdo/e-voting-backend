const crypto = require('crypto').webcrypto;
const fs = require('fs');
const importPrivateKey = require('./ImportPrivateKey');

const PATH_PUBLIC = './app/crypto/keys/PublicKey.txt';
const PATH_PRIVATE = './app/crypto/keys/PrivateKey.txt';

async function Decrypt(data){
    const keyString = fs.readFileSync(PATH_PRIVATE, {encoding: 'utf8', flag: 'r'});
    const key = await importPrivateKey(keyString);
    return crypto.subtle.decrypt(
        {
            name: "RSA-OAEP"
        },
        key,
        data
    );
}

module.exports = Decrypt;
