const {Model, DataTypes} = require('sequelize');
const sequelize = require('./index');

class Log extends Model {}

Log.init({
    userId: {
        type: DataTypes.INTEGER
    },
    log: {
        type: DataTypes.STRING
    }
},{
    sequelize,
    modelName: 'log',
    freezeTableName: true,
    timestamps: false
})

module.exports = Log;