const crypto = require('crypto').webcrypto;

async function importUserPublicKey(publicKeyString){
    const publicKeyJSON = JSON.parse(publicKeyString);
    return await crypto.subtle.importKey(
        "jwk",
        publicKeyJSON,
        {
            name: "ECDSA",
            namedCurve: "P-384"
        },
        true,
        ["verify"]);
}

module.exports = importUserPublicKey;