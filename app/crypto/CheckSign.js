const crypto = require('crypto').webcrypto;
const importUserPublicKey = require('./ImportUserPublicKey');

async function CheckSign(userPublicKeyString, userData, userSignature){
    try{
        const key = await importUserPublicKey(userPublicKeyString);
        let enc = new TextEncoder();
        const encData = enc.encode(userData);
        const sign = Uint8Array.from(userSignature);
        return await crypto.subtle.verify(
            {
                name: "ECDSA",
                hash: {name: "SHA-384"},
            },
            key,
            sign,
            encData
        )
    }catch{
        return false;
    }
}

module.exports = CheckSign;


