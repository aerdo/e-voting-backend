const { Sequelize } = require('sequelize')

const sequelize = new Sequelize('eVoting-db', 'user', 'password',{
    dialect: 'sqlite',
    host: './dev.sqlite'
})

module.exports = sequelize;