const crypto = require('crypto').webcrypto;

async function importPrivateKey(privateKeyString){
    const privateKeyJSON = JSON.parse(privateKeyString);
    return await crypto.subtle.importKey("jwk",
        privateKeyJSON,
        {
            name: "RSA-OAEP",
            modulusLength: 4096,
            publicExponent: new Uint8Array([1, 0, 1]),
            hash: "SHA-256"
        },
        true,
        ["decrypt"]);
}

module.exports = importPrivateKey;
